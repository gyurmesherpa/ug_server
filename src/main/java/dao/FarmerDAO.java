/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.List;
import java.util.Map;
import models.AnimalIssue;
import models.FarmDetails;
import models.Transaction;

/**
 *
 * @author gyurme
 */
public interface FarmerDAO {
    int registerFarmer(FarmDetails fd);
    FarmDetails getFarmDetails(String email);
    int backupTransactions(List<Transaction> transToBackup);
    List<String> getAllAnimals();
    List<String> getAllAnimalDiseases(String animal);
    int recordReportedIncident(String farmId, String reportDate, String incidentDate, String animal, String issue, String symptoms, String subject);
    List<AnimalIssue> getDiseasesAlerts(String farmId);
    double getNetCashInflow(String farmId);
    double getNetCashOutflow(String farmId);
}

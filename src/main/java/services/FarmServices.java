/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import dao.FarmerDAO;
import daoimpl.FarmerDAOImpl;
import java.util.List;
import models.FarmDetails;
import models.Transaction;

/**
 *
 * @author gyurme
 */
public class FarmServices {
    public int registerFarm(FarmDetails fd){
        FarmerDAO fdao = new FarmerDAOImpl();
        return fdao.registerFarmer(fd);
    }
    
    public FarmDetails getFarmDetails(String email){
        FarmerDAO fdao = new FarmerDAOImpl();
        return fdao.getFarmDetails(email);
    }
    
    public int backupTransactions(List<Transaction> transToBackup){
        FarmerDAO fdao = new FarmerDAOImpl();
        return fdao.backupTransactions(transToBackup);
    }
    
    public String financialAnalysis(String farmId){
        FarmerDAO fdao = new FarmerDAOImpl();
        double totalInflow = fdao.getNetCashInflow(farmId);
        double totalOutflow = fdao.getNetCashOutflow(farmId);
        double netcashInflow = totalInflow - totalOutflow;
        String analysis = "";
        if(netcashInflow > 0){
            analysis = "Congratulations! Your business seems to have net cash inflow";
        }
        else{
            analysis = "Sorry! Your business seems to be at a loss";
        }
        String report = "Total cash inflow:\n"+totalInflow+"\n\nTotal cash outflow:\n"+totalOutflow+"\n\nNet cash inflow:\n"+netcashInflow+"\n\n\nAnalysis:"+analysis;
        return report;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author gyurme
 */
public class FarmDetails {
    String farmServerId;
    String farmName;
    String farmEmail;
    String farmPwd;
    String farmPhoneNo;
    String district;
    String plusCode;
    private String latitude;
    private String longitude;

    public String getFarmServerId() {
        return farmServerId;
    }

    public void setFarmServerId(String farmServerId) {
        this.farmServerId = farmServerId;
    }

    public String getFarmName() {
        return farmName;
    }

    public void setFarmName(String farmName) {
        this.farmName = farmName;
    }

    public String getFarmEmail() {
        return farmEmail;
    }

    public void setFarmEmail(String farmEmail) {
        this.farmEmail = farmEmail;
    }

    public String getFarmPwd() {
        return farmPwd;
    }

    public void setFarmPwd(String farmPwd) {
        this.farmPwd = farmPwd;
    }

    public String getFarmPhoneNo() {
        return farmPhoneNo;
    }

    public void setFarmPhoneNo(String farmPhoneNo) {
        this.farmPhoneNo = farmPhoneNo;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getPlusCode() {
        return plusCode;
    }

    public void setPlusCode(String plusCode) {
        this.plusCode = plusCode;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    
    
    
    
    
    
}

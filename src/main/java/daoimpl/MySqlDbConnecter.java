/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoimpl;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author gyurme
 */
public class MySqlDbConnecter {
    public static Connection limitedPriviligeConnectDb(){
        String urlAndPort = "localhost:3306";
	String dbName = "farm_ug";
	String username = "root";		
        String pwd = "";
	Connection conn = null;
	try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://"+urlAndPort+"/"+dbName,username,pwd);
	} 
        catch (Exception e) {
            e.printStackTrace();
        }
	return conn;
    }
}

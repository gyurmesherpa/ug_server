/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daoimpl;

import dao.FarmerDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import models.AnimalIssue;
import models.FarmDetails;
import models.Transaction;

/**
 *
 * @author gyurme
 */
public class FarmerDAOImpl implements FarmerDAO{
    
    @Override
    public int registerFarmer(FarmDetails fd) {
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
        String sql = "insert into farm_details(farm_name,farm_email,farm_pwd,farm_phone,farm_id,district,farm_google_plus_code) values(?,?,?,?,?,?,?)";
        //String selectSql = "SELECT branch_id, branch_name, location FROM branches WHERE branch_id =(SELECT MAX(branch_id) FROM branches);";
        int rowsAffected = -1;
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, fd.getFarmName());
            ps.setString(2, fd.getFarmEmail());
            ps.setString(3, fd.getFarmPwd());
            ps.setString(4, fd.getFarmPhoneNo());
            ps.setString(5, fd.getFarmServerId());
            ps.setString(6, fd.getDistrict());
            ps.setString(7, fd.getPlusCode());
            rowsAffected = ps.executeUpdate(); //returns no of rows affected for DML statements
            ps.close();
            conn.close();
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return rowsAffected;
    }

    @Override
    public FarmDetails getFarmDetails(String email) {
        String sql = "select * from farm_details where farm_email = '" + email + "';";
        FarmDetails fd = new FarmDetails();
        //............
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
       
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                fd.setFarmServerId(rs.getString("farm_id"));
                fd.setFarmEmail(rs.getString("farm_email"));
                fd.setFarmName(rs.getString("farm_name"));
                fd.setFarmPwd(rs.getString("farm_pwd"));
                fd.setFarmPhoneNo(rs.getString("farm_phone"));
                fd.setLatitude(rs.getString("lat_cor"));
                fd.setLongitude(rs.getString("lon_cor"));
                fd.setPlusCode(rs.getString("farm_google_plus_code"));
                
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return fd;
    }

    @Override
    public int backupTransactions(List<Transaction> transToBackup) {
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
        String sql = "insert into transactions(farm_id,date_time,transaction_type,bill,name,reciept_amount,payment_amount,explanation) values";
        int rowsCount = 0;
        for(int i = 0; i < transToBackup.size(); i++){
            Transaction t = transToBackup.get(i);
            if(i > 0){
                sql = sql + ",('"+ t.getFarmServerId() + "', '" + new java.sql.Timestamp(Long.valueOf(t.getDateTime())) + "', '" + (t.getTransactionType()).replaceAll("'","\\\\'") + "', '" + t.getBill()+ "', '" + (t.getTransactionName()).replaceAll("'","\\\\'")+ "', '" + t.getRecieptAmount()+ "','" + t.getPaymentAmount() + "','" + (t.getTransactionDetails()).replaceAll("'","\\\\'")+ "')";
            }
            else{
                sql = sql + "('"+ t.getFarmServerId() + "', '" + new java.sql.Timestamp(Long.valueOf(t.getDateTime())) + "', '" + (t.getTransactionType()).replaceAll("'","\\\\'") + "', '" + t.getBill()+ "', '" + (t.getTransactionName()).replaceAll("'","\\\\'")+ "', '" + t.getRecieptAmount()+ "','" + t.getPaymentAmount() + "','" + (t.getTransactionDetails()).replaceAll("'","\\\\'")+ "')";
            }
        }
        try {
            Statement st = conn.createStatement();
            rowsCount = st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(FarmerDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println(sql);
        return rowsCount;
    }

    @Override
    public List<String> getAllAnimals() {
        String sql = "select * from animals where animal <> 'any_animal_bird';";
        List<String> animalsList = new ArrayList<>();
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
       animalsList.add("choose animal");
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                System.out.println("next found");
                animalsList.add(rs.getString("animal"));
                
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return animalsList;
    }

    @Override
    public List<String> getAllAnimalDiseases(String animal) {
        String sql = "select issue from animal_related_issues where animal = '" + animal + "';";
        List<String> animalDiseasesList = new ArrayList<>();
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
       
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            animalDiseasesList.add("Choose Issue");
            animalDiseasesList.add("Unconfirmed Issue");
            while(rs.next()){
                System.out.println("next found");
                animalDiseasesList.add(rs.getString("issue"));
                
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return animalDiseasesList;
    }

    @Override
    public int recordReportedIncident(String farmId, String reportDate, String incidentDate, String animal, String issue, String symptoms, String subject) {
        int rowsAffected = 0;
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
        String sql = "insert into reported_animal_related_issues(reporter_farm_id, animal, reporter_suspected_issue, reported_symptoms, report_subject, reported_at, incident_of_or_since) values(?,?,?,?,?,?,?)";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1,farmId);
            ps.setString(2, animal);
            ps.setTimestamp(6, new java.sql.Timestamp(Long.valueOf(reportDate)));
            ps.setString(7, incidentDate);
            ps.setString(4, symptoms);
            ps.setString(3, issue);
            ps.setString(5, subject);
            rowsAffected = ps.executeUpdate();
            ps.close();
            conn.close();
            
        }catch(SQLException e){
            e.printStackTrace();
        }
        return rowsAffected;
    }

    @Override
    public List<AnimalIssue> getDiseasesAlerts(String farmId) {
        String sql = "SELECT animal, expert_issue_confirmation, report_subject, reported_at, incident_of_or_since, farm_details.farm_google_plus_code, farm_details.district, farm_details.lat_cor, farm_details.lon_cor FROM reported_animal_related_issues, farm_details WHERE (reporter_farm_id = farm_details.farm_id AND reported_animal_related_issues.expert_issue_confirmation IS NOT NULL AND farm_details.farm_id <> '"+farmId+"');";
        List<AnimalIssue> diseasePoints = new ArrayList<>();
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
       
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            //animal 	expert_issue_confirmation 	report_subject 	reported_at 	incident_of_or_since 	farm_google_plus_code 	district 	
            while(rs.next()){
                AnimalIssue as = new AnimalIssue();
                as.setAnimal(rs.getNString("animal"));
                as.setDistrict(rs.getString("district"));
                as.setConfirmedIssue(rs.getString("expert_issue_confirmation"));
                as.setIssueName(rs.getString("report_subject")); // as reported
                as.setReportedAt(rs.getString("reported_at"));
                as.setIncidentOf(rs.getString("incident_of_or_since"));
                as.setPlusCode(rs.getString("farm_google_plus_code"));
                as.setDistrict(rs.getString("district"));
                as.setLatitude(rs.getString("lat_cor"));
                as.setLongitude(rs.getString("lat_cor"));
                diseasePoints.add(as);
                
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return diseasePoints;
    }

    @Override
    public double getNetCashInflow(String farmId) {
        String sql = "select payment_amount from transactions where farm_id = '"+farmId+"';";
        double totalOutamount = 0.0;
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
       
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                totalOutamount = totalOutamount + Double.valueOf(rs.getString("payment_amount"));
                
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return totalOutamount;
    }

    @Override
    public double getNetCashOutflow(String farmId) {
        String sql = "select reciept_amount from transactions where farm_id = '"+farmId+"';";
        double totalInamount = 0.0;
        Connection conn = MySqlDbConnecter.limitedPriviligeConnectDb();
       
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                totalInamount = totalInamount + Double.valueOf(rs.getString("reciept_amount"));
                
            }
            rs.close();
            conn.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        return totalInamount;
    }
    
}

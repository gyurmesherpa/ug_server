/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gyurmeug.gyurmeugserver.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import dao.FarmerDAO;
import daoimpl.FarmerDAOImpl;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import models.FarmDetails;
import models.Transaction;
import services.FarmServices;

/**
 *
 * @author gyurme
 */
@WebServlet(name = "FarmerRegister", urlPatterns = {"/farm"})
public class Farm extends HttpServlet {

    /**
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("get request arrived");
        String responseText = "Hello";
            try ( PrintWriter out = response.getWriter()) {
                out.println(responseText);
            }        
        
    }    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("post request arrived");
        Enumeration<String> requestParameters = request.getParameterNames();
        while (requestParameters.hasMoreElements()) {
            String paramName = (String) requestParameters.nextElement();
            System.out.println("Request Paramter Name: " + paramName 
                            + ", Value - " + request.getParameter(paramName));
        }
        String responseText;
        if(request.getParameter("r").equals("register_farm")){
            FarmServices fs = new FarmServices();
            FarmDetails fd = new FarmDetails();
            String farmId = "f" + new Date().getTime();
            fd.setFarmServerId(farmId);
            fd.setFarmEmail(request.getParameter("email"));
            fd.setFarmName(request.getParameter("farm_name"));
            fd.setFarmPwd(request.getParameter("pwd"));
            fd.setFarmPhoneNo(request.getParameter("phone_no"));
            fd.setDistrict(request.getParameter("district"));
            fd.setPlusCode(request.getParameter("plus_code"));
            responseText = String.valueOf(fs.registerFarm(fd));
            try ( PrintWriter out = response.getWriter()) {
                out.println(responseText);
            }
        }
        else if(request.getParameter("r").equals("get_firm_account_details")){
            FarmServices fs = new FarmServices();
            FarmDetails fdetails = fs.getFarmDetails(request.getParameter("email"));
            if(fdetails.getFarmServerId() == null){
                responseText = "no account with this email";
            }
            else{
                ObjectMapper om = new ObjectMapper();
                responseText = om.writeValueAsString(fdetails);
            }
            
            System.out.println(responseText);
            try ( PrintWriter out = response.getWriter()) {
                out.println(responseText);
            }
        }
        else if(request.getParameter("r").equals("backup_transactions")){
            FarmServices fs = new FarmServices();
            ObjectMapper om = new ObjectMapper();
            List<Transaction> transactionsToBackup = om.readValue(request.getParameter("transactions"), om.getTypeFactory().constructCollectionType(List.class, Transaction.class));
            //System.out.println(transactionsToBackup.get(0).getTransactionName());
            
            try ( PrintWriter out = response.getWriter()) {
                if(fs.backupTransactions(transactionsToBackup) == transactionsToBackup.size()){
                    out.println("Backup Successful");
                }
                else{
                    out.println("Backup failed");
                }
                
            }
        }
        else if(request.getParameter("r").equals("get_all_animals_list")){
            FarmServices fs = new FarmServices();
            ObjectMapper om = new ObjectMapper();
            FarmerDAO fd = new FarmerDAOImpl();
            ObjectMapper omapper = new ObjectMapper();
            try ( PrintWriter out = response.getWriter()) {
                out.println(omapper.writeValueAsString(fd.getAllAnimals()));                
            }
        }
        else if(request.getParameter("r").equals("get_specific_animal_diseases")){
            String animal = request.getParameter("animal");
            FarmerDAO fd = new FarmerDAOImpl();
            ObjectMapper omapper = new ObjectMapper();
            try ( PrintWriter out = response.getWriter()) {
                out.println(omapper.writeValueAsString(fd.getAllAnimalDiseases(animal)));                
            }
        }
        else if(request.getParameter("r").equals("report_farm_incident")){
            String farmId = request.getParameter("reporting_farm_id");
            String reportDate = request.getParameter("reporting_date");
            String incidentDate = request.getParameter("incident_of_or_since");
            String animal = request.getParameter("animal");
            String issue = request.getParameter("issue");
            String symptoms = request.getParameter("symptoms");
            String subject = request.getParameter("report_subject");
            FarmerDAO fd = new FarmerDAOImpl();
            int success = fd.recordReportedIncident(farmId, reportDate, incidentDate, animal, issue, symptoms, subject);
            
            try ( PrintWriter out = response.getWriter()) {
                if(success == 1){
                    //System.out.println(success);
                    out.println("successfully reported");      
                }
                else{
                    out.println("reporting failed");   
                }
                          
            }
        }
        else if(request.getParameter("r").equals("farm_incident_alerts")){
            ObjectMapper omapper = new ObjectMapper();
            FarmerDAO fd = new FarmerDAOImpl();
            System.out.println(omapper.writeValueAsString(fd.getDiseasesAlerts(request.getParameter("requester_farm_id"))));
            try ( PrintWriter out = response.getWriter()) {
                out.println(omapper.writeValueAsString(fd.getDiseasesAlerts(request.getParameter("requester_farm_id"))));                          
            }              
        }
        else if(request.getParameter("r").equals("get_financial_analysis")){
            FarmServices fs = new FarmServices();
            String analysisText = fs.financialAnalysis(request.getParameter("requester_farm_id"));
            System.out.println(analysisText);
            try ( PrintWriter out = response.getWriter()) {
                out.println(analysisText);                          
            }
        }
    }

    /**
     * 
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

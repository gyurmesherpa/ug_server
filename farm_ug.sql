-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 04, 2021 at 12:55 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `driving`
--
CREATE DATABASE IF NOT EXISTS `driving` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `driving`;

-- --------------------------------------------------------

--
-- Table structure for table `addblog`
--

CREATE TABLE `addblog` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(9999) CHARACTER SET utf32 COLLATE utf32_german2_ci NOT NULL,
  `image` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `addblog`
--

INSERT INTO `addblog` (`id`, `title`, `content`, `image`) VALUES
(7, 'Bike and Scooter Trial Test', 'Bikes and Scooter trial has similar kind of trial system expect few. Bike and Scooter has also starting eight shaped field trial...', 'mb test.PNG'),
(8, 'Car Eight Trial Test:', 'First step is to pass the eight shaped field whithout touching traffic cone poles. Need to drive slowly and enter inside eight shaped field from left hand side, drive slowly...\r\n\r\n', 'car test.PNG'),
(9, 'Car Zebra Crossing, Street Breaker Test:', 'After you complete the eight trial, then you will have to drive from zebra crossing. When you reach near zebra crossing, there will be a traffic pole...', 'light test.PNG'),
(10, 'Car Inclined Plane Test:', 'After Zebra Crossing, Street Breaker and Traffic Light Test, it comes inclined plane test. Slowly move forward on inclined plane, cross the line marked and land the car...', 'ukalo test.PNG'),
(11, 'Car T Parking Test:', 'Final test is T Parking Test, once you pass this test, all trial is over, you will be eligible to get driving license. This test is also as important as other test...\r\n\r\n', 'Tparking test.PNG');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `contact` int(250) NOT NULL,
  `timing` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `firstname`, `lastname`, `email`, `contact`, `timing`) VALUES
(22, 'bibek123', 'tripathi', 'kripapandey3@gmail.com', 798, 'noon'),
(23, 'Kripa', 'tripathii', 'kripapandey3@gmail.com', 2147483647, 'noon'),
(24, 'Trista', 'Khadka', 'khadka.treesta12@gmail.com', 2147483647, 'morning'),
(25, 'Trista', 'Khadka', 'khadka.treesta12@gmail.com', 2147483647, 'morning');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `description` varchar(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `subject` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `description`, `name`, `email`, `subject`) VALUES
(1, 'dsad', 'kripa', 'iambebeck@gmail.com', 'fgdgergergers'),
(2, 'dasdsa', 'new professional motor driving', 'pandeyaava@gmail.com', 'fgdgergergers'),
(3, 'Hello', 'Rohit', 'ritesh@gmail.com', 'Test'),
(4, 'hellow this is gyurme.', 'Gyurme Sherpa', 'gyurme.sp@gmail.com', 'hello'),
(5, 'hejsjs', 'Gyurme Sherpa', 'gs@gmail.com', 'gggg');

-- --------------------------------------------------------

--
-- Table structure for table `institute`
--

CREATE TABLE `institute` (
  `id` int(250) NOT NULL,
  `name` varchar(250) NOT NULL,
  `location` varchar(250) NOT NULL,
  `contact` bigint(250) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `specialisedin` varchar(250) NOT NULL,
  `rates` bigint(250) NOT NULL,
  `userid` int(250) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institute`
--

INSERT INTO `institute` (`id`, `name`, `location`, `contact`, `description`, `specialisedin`, `rates`, `userid`, `photo`) VALUES
(10, 'okay', 'kusunti', 123, 'vcasvdaca', 'motorbike', 2000, 15, ''),
(12, 'new professional motor driving', 'dhobighat', 9841701979, 'asxascdscddcd', 'car', 12000, 15, 'car test.PNG'),
(13, 'banglamukhi driving', 'dhobighat', 123, 'dbbvefuvebevnefjv', 'car', 12000, 15, 'car test.PNG'),
(14, 'manakamana ', 'pulchowk', 123, 'xbasbcajbcdjb', 'car', 12000, 17, 'car test.PNG'),
(15, 'Ribhu', 'Kanibhal,lalitpur', 9823, 'Hello', 'car', 12000, 20, 'Pic_636575724009678407.jpg'),
(16, 'new professional motor driving', 'pulchowk', 9841701979, 'xbasbcajbcdjb', 'motorbike', 12000, 20, 'Pic_636575723579920439.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `upload`
--

CREATE TABLE `upload` (
  `id` int(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `upload`
--

INSERT INTO `upload` (`id`, `photo`) VALUES
(0, 'result.PNG'),
(0, ''),
(0, ''),
(0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(20) NOT NULL,
  `firstname` varchar(250) NOT NULL,
  `lastname` varchar(250) NOT NULL,
  `num` bigint(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `type` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `photo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `num`, `email`, `type`, `password`, `photo`) VALUES
('jid32', 'gyurme', 'gyurme', 456, 'gyurme.sp@gmail.com', '1', '243e61e9410a9f577d2d662c67025ee9', ''),
('uid33', 'gyurme', 'sherpa', 322, 'gs@gmail.com', '1', '243e61e9410a9f577d2d662c67025ee9', NULL),
('user34', 'kripa', 'pandey', 4343, 'k@gmail.com', '1', '243e61e9410a9f577d2d662c67025ee9', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `addblog`
--
ALTER TABLE `addblog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute`
--
ALTER TABLE `institute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `addblog`
--
ALTER TABLE `addblog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `institute`
--
ALTER TABLE `institute`
  MODIFY `id` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- Database: `ecommerce`
--
CREATE DATABASE IF NOT EXISTS `ecommerce` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `ecommerce`;

-- --------------------------------------------------------

--
-- Table structure for table `comment_types`
--

CREATE TABLE `comment_types` (
  `type_id` varchar(15) NOT NULL,
  `type_description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment_types`
--

INSERT INTO `comment_types` (`type_id`, `type_description`) VALUES
('comment', 'not reply'),
('reply', 'not direct comment');

-- --------------------------------------------------------

--
-- Table structure for table `item_category`
--

CREATE TABLE `item_category` (
  `category_id` varchar(100) NOT NULL,
  `description` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_category`
--

INSERT INTO `item_category` (`category_id`, `description`) VALUES
('Car', NULL),
('Mobile', '0'),
('Motorcycle', '0'),
('Scooter', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_comments`
--

CREATE TABLE `item_comments` (
  `comment_id` varchar(30) NOT NULL,
  `datetime` int(11) NOT NULL,
  `comment` varchar(200) NOT NULL,
  `item_id` varchar(30) NOT NULL,
  `comment_type_id` varchar(15) NOT NULL,
  `commenter` varchar(30) NOT NULL,
  `commenter_name` varchar(50) NOT NULL,
  `reply_to` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_comments`
--

INSERT INTO `item_comments` (`comment_id`, `datetime`, `comment`, `item_id`, `comment_type_id`, `commenter`, `commenter_name`, `reply_to`) VALUES
('1607591003', 2020, 'testttttt', 'pid-1606466010', 'comment', 'uid-1602835691', 'gyurme', NULL),
('1607591451', 2020, 'exchange with opp v9??', 'pid-1606466010', 'comment', 'uid-1602835691', 'gyurme', NULL),
('1608391909', 2020, 'Exchange with Iphone?', 'pid-1606466010', 'comment', 'uid-1608391869', 'Bishal', NULL),
('1608392014', 2020, 'ok', 'pid-1606466010', 'comment', 'uid-1608391869', 'Bishal', NULL),
('1608392481', 2020, 'exchange with Samsung?', 'pid-1606466010', 'comment', 'uid-1608391869', 'Bishal', NULL),
('1608401390', 2020, 'okokok', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392014'),
('1608401754', 2020, 'which model?', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1608404061', 2020, 'noooooooo', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591451'),
('1608404072', 2020, 'which series?', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608391909'),
('1608406371', 2020, 'repliessss...', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1608406389', 2020, 'no i dont want iphone', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608391909'),
('1608406461', 2020, 'reply 2', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1608407147', 2020, 'noooo', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392014'),
('1608407189', 2020, 'not intrested in oppo. sorry.', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591451'),
('1608659249', 2020, 'how much warranty left?', 'pid-1606466010', 'comment', 'uid-1608659204', 'Asik', NULL),
('1609007986', 2020, 'Is everything okay?', 'pid-1604686451', 'comment', 'uid-1608659204', 'Asik', NULL),
('1609153581', 2020, 'Is it in good condition?', 'pid-1606460612', 'comment', 'uid-1603199592', 'Ritesh', NULL),
('1609153600', 2020, 'Wanna exchange with samsun s7?', 'pid-1606460612', 'comment', 'uid-1608659204', 'Asik', NULL),
('1609414804', 2020, 'How much kilometer this bike ran?', 'pid-1609414758', 'comment', 'uid-1609180623', 'Ram', NULL),
('1609525830', 2021, 'Is it in good condition?', 'pid-1609524392', 'comment', 'uid-1602835691', 'gyurme', NULL),
('1609525855', 2021, 'Yes, Evrything is fine.', 'pid-1609524392', 'reply', 'uid-1609180623', 'Ram', '1609525830'),
('1609525873', 2021, 'Yes Everything is Fine.', 'pid-1609524392', 'reply', 'uid-1609180623', 'Ram', '1609525830'),
('1609944928', 2021, 'reply 3', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608392481'),
('1609944990', 2021, '5 months\n', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608659249'),
('1609945001', 2021, '5 month', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608659249'),
('1609945010', 2021, 'test 2', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591003'),
('1609945016', 2021, 'test 2', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591003'),
('1609945022', 2021, 'test3', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1607591003'),
('1609945036', 2021, '6 month', 'pid-1606466010', 'reply', 'uid-1602835691', 'gyurme', '1608659249'),
('1609956355', 2021, 'Is it on good condition?', 'pid-1609956246', 'comment', 'uid-1603199592', 'Ritesh', NULL),
('1609956373', 2021, 'Yes perfectly fine', 'pid-1609956246', 'reply', 'uid-1602835691', 'gyurme', '1609956355'),
('1609956382', 2021, 'yes perfectly fine', 'pid-1609956246', 'reply', 'uid-1602835691', 'gyurme', '1609956355'),
('1609957052', 2021, 'Is everything fine?', 'pid-1609957035', 'comment', 'uid-1609180623', 'Ram', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `on_sale_items`
--

CREATE TABLE `on_sale_items` (
  `item_id` varchar(30) NOT NULL,
  `adv_title` varchar(50) NOT NULL,
  `item_images_json` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL,
  `price` int(11) NOT NULL,
  `sold_status` varchar(30) DEFAULT NULL,
  `date_added` timestamp NOT NULL DEFAULT current_timestamp(),
  `seller` varchar(30) NOT NULL,
  `category_id` varchar(30) NOT NULL,
  `date_sold` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `price_negotiability` varchar(3) NOT NULL,
  `brand_name` varchar(50) DEFAULT NULL,
  `description` text NOT NULL,
  `accepted_order_id` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `on_sale_items`
--

INSERT INTO `on_sale_items` (`item_id`, `adv_title`, `item_images_json`, `price`, `sold_status`, `date_added`, `seller`, `category_id`, `date_sold`, `price_negotiability`, `brand_name`, `description`, `accepted_order_id`) VALUES
('pid-1603994085', 'nifnosi', '\"advertiseImage/pid-1603994085.png\"', 5151, NULL, '2020-10-29 13:09:45', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Hunk', '', NULL),
('pid-1604165109', 'Bike on Sell', '\"advertiseImage/pid-1604165109.jpg\"', 51651, NULL, '2020-10-31 12:40:09', 'uid-1603128626', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Hunk', '', NULL),
('pid-1604165222', 'Scooter on Sell', '\"advertiseImage/pid-1604165222.png\"', 51666, 'order_placed', '2020-10-31 12:42:02', 'uid-1603128626', 'Scooter', '0000-00-00 00:00:00', 'No', 'Pleasure', '', NULL),
('pid-1604165255', 'Car on Sell', '\"advertiseImage/pid-1604165255.jpg\"', 145315, NULL, '2020-10-31 12:42:35', 'uid-1603128626', 'Car', '0000-00-00 00:00:00', 'No', 'Scorpio', '', NULL),
('pid-1604173500', 'Seeling my Mobile', '\"advertiseImage/pid-1604173500.jpg\"', 56416, NULL, '2020-10-31 15:00:00', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'Pixel', '', '1609149805'),
('pid-1604173545', 'Seeling my Bike', '\"advertiseImage/pid-1604173545.jpg\"', 564848, 'order_placed', '2020-10-31 15:00:45', 'uid-1602835691', 'Car', '0000-00-00 00:00:00', 'No', 'Scorpio', '', NULL),
('pid-1604686451', 'Sccoter On sale', '\"advertiseImage/pid-1604686451.jpg\"', 154780, NULL, '2020-11-06 13:29:11', 'uid-1602835691', 'Scooter', '0000-00-00 00:00:00', 'Yes', 'Pleasure', '', '1609009400'),
('pid-1606460612', 'Description testing', '\"advertiseImage/pid-1606460612.jpg\"', 15151515, 'order_placed', '2020-11-27 02:18:32', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'OnePlus', 'Description testinhgggggg', NULL),
('pid-1606466010', 'One plus mobile on sale.', '\"advertiseImage/pid-1606466010.jpg\"', 35000, 'order_placed', '2020-11-27 03:48:30', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'No', 'OnePlus', 'I want to sell my one plus mobile as i have plan to buy new phone after selling this mobile. Mobile is ok no damage. Warrant card is also available(4 month remaining). Also with back cover.', NULL),
('pid-1608485821', 'immmmm', '\"advertiseImage/pid-1608485821.jpg\"', 2, 'order_placed', '2020-12-20 12:52:01', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'OnePlus', 'ajkdakjndakasasasasasasasasasasa', '1609148579'),
('pid-1609182796', 'For order status testing', '\"advertiseImage/pid-1609182796.png\"', 123, 'order_placed', '2020-12-28 14:28:16', 'uid-1608659204', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'Pixel', 'qqq', '1609182825'),
('pid-1609325271', 'for order test', '\"advertiseImage/pid-1609325271.jpg\"', 123333333, 'order_delivered', '2020-12-30 06:02:51', 'uid-1602835691', 'Car', '0000-00-00 00:00:00', 'Yes', 'Mazda', 'qqqqqqq', '1609325295'),
('pid-1609330913', 'qqqqq', '\"advertiseImage/pid-1609330913.png\"', 2147483647, 'order_delivered', '2020-12-30 07:36:53', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Splender', 'wdqada', '1609330939'),
('pid-1609414758', 'Splender on Sale', '\"advertiseImage/pid-1609414758.jpg\"', 45000, 'order_delivered', '2020-12-31 06:54:18', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Splender', 'Splender Bike is on sale. All taxes is paid. Bike is on perfect condition.', '1609414840'),
('pid-1609521772', 'Mobile on sale', '\"advertiseImage/pid-1609521772.png\"', 35000, 'order_delivered', '2021-01-01 12:37:52', 'uid-1602835691', 'Mobile', '0000-00-00 00:00:00', 'No', 'Iphone', 'Mobile on sale. Like brand new', '1609521814'),
('pid-1609524392', 'Motorcycle on sale', '\"advertiseImage/pid-1609524392.jpg\"', 350000, NULL, '2021-01-01 13:21:32', 'uid-1609180623', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'Beneli', 'Bike on sale for urgent purpose.', NULL),
('pid-1609956095', 'security testtttt', '\"advertiseImage/pid-1609956095.jpg\"', 122112, NULL, '2021-01-06 13:16:35', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'pulsar', 'aaa', NULL),
('pid-1609956246', 'Bike on sale', '\"advertiseImage/pid-1609956246.jpg\"', 55554, 'order_delivered', '2021-01-06 13:19:06', 'uid-1602835691', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'R15', 'mjjj', '1609956273'),
('pid-1609956825', 'Ritesh Ad', '\"advertiseImage/pid-1609956825.jpg\"', 45000, 'order_delivered', '2021-01-06 13:28:45', 'uid-1603199592', 'Car', '0000-00-00 00:00:00', 'No', 'Mazda', 'aaa', '1609956850'),
('pid-1609957035', 'rttt', '\"advertiseImage/pid-1609957035.jpg\"', 123324444, 'order_delivered', '2021-01-06 13:32:15', 'uid-1603199592', 'Mobile', '0000-00-00 00:00:00', 'Yes', 'Redimi', 'aa', '1609957061'),
('pid-1609957652', 'Mobile On sale', '\"advertiseImage/pid-1609957652.jpg\"', 144788, 'order_delivered', '2021-01-06 13:42:32', 'uid-1603199592', 'Mobile', '0000-00-00 00:00:00', 'No', 'LG', 'aaaa', '1609957687'),
('pid-1609957894', 'aaaa', '\"advertiseImage/pid-1609957894.jpg\"', 111, NULL, '2021-01-06 13:46:34', 'uid-1603199592', 'Motorcycle', '0000-00-00 00:00:00', 'Yes', 'R15', 'aa', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `order_msg` varchar(500) NOT NULL,
  `order_acceptence_msg` varchar(500) DEFAULT NULL,
  `buyer_id` varchar(30) NOT NULL,
  `ad_id` varchar(30) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `rating` int(1) DEFAULT NULL,
  `feedback` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `date`, `order_msg`, `order_acceptence_msg`, `buyer_id`, `ad_id`, `status`, `rating`, `feedback`) VALUES
('1608918681', '2020-12-25 13:06:21', 'i liked your order. i am ready to buy in 35000nrs. contact me.', NULL, 'uid-1608918638', 'pid-1606466010', 'order_placed', NULL, NULL),
('1608919918', '2020-12-25 13:26:58', 'testtttttttttttt', NULL, 'uid-1608918638', 'pid-1606466010', 'order_placed', NULL, NULL),
('1608919925', '2020-12-25 13:27:05', 'wwwwwwwww', NULL, 'uid-1608918638', 'pid-1606466010', 'order_placed', NULL, NULL),
('1609009400', '2020-12-26 14:18:20', 'i m ready to buy contact me.', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609009432', '2020-12-26 14:18:52', 'i m ready to buy contact me. Let\'s talk.', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609009521', '2020-12-26 14:20:21', 'i m ready to buy contact me. Let\'s talk. 2222', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609148579', '2020-12-28 04:57:59', 'I would like buy this mobile in this price.', NULL, 'uid-1608659204', 'pid-1608485821', 'order_placed', NULL, NULL),
('1609149745', '2020-12-28 05:17:25', 'Ok I am ready to buy this scooter.', NULL, 'uid-1608659204', 'pid-1604686451', 'order_placed', NULL, NULL),
('1609149805', '2020-12-28 10:03:48', 'Ok I am ready to buy...', 'ok contact mee. my number 4565165', 'uid-1608659204', 'pid-1604173500', 'order_placed', NULL, NULL),
('1609153627', '2020-12-28 11:25:47', 'Ok brother i want ti buy this phone please give me your contact details.', 'ok here is my contact number. 846515', 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609153676', '2020-12-28 06:22:56', 'Ok I am ready to buy this phone by tommorow. Full cash deal.', NULL, 'uid-1603199592', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609153859', '2020-12-28 06:25:59', 'Ok I am ready to buy this phone by tommorow. Full cash dealbdaiudnhaidjba', NULL, 'uid-1603199592', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609153891', '2020-12-28 06:26:31', 'Ok I am ready to buy this phone by tommorow. Full cash dealbdaiudnhaidjba', NULL, 'uid-1603199592', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609155962', '2020-12-28 07:01:02', 'hhhhhhhhhhhhhhhhhh', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609155978', '2020-12-28 07:01:18', 'hhhhhhhhhhhhhhhhhh', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609156144', '2020-12-28 07:04:04', 'weyfguwbiufcwojs', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609156266', '2020-12-28 07:06:06', 'weyfguwbiufcwojs', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609156376', '2020-12-28 07:07:56', 'weyfguwbiufcwojs', NULL, 'uid-1608659204', 'pid-1606460612', 'order_placed', NULL, NULL),
('1609175521', '2020-12-28 12:27:01', 'test1', NULL, 'uid-1608659204', 'pid-1604173545', 'order_placed', NULL, NULL),
('1609180651', '2020-12-28 13:52:31', 'I would like to buy.', NULL, 'uid-1609180623', 'pid-1604165222', 'order_placed', NULL, NULL),
('1609181786', '2020-12-28 14:11:26', 'so cheap i want to buy right now.', NULL, 'uid-1609180623', 'pid-1608485821', 'order_placed', NULL, NULL),
('1609182825', '2020-12-28 19:14:25', 'for testingggggggggg', 'ok lets do deal.', 'uid-1609180623', 'pid-1609182796', 'order_placed', NULL, NULL),
('1609324525', '2020-12-30 05:50:25', 'i would like to buy.', NULL, 'uid-1602835691', 'pid-1609182796', 'order_placed', NULL, NULL),
('1609324819', '2020-12-30 05:55:19', 'dkjabdlkandlk', NULL, 'uid-1609180623', 'pid-1606466010', 'order_placed', NULL, NULL),
('1609325295', '2021-01-01 15:30:17', 'Ok i would like to buy.', 'Ok then here is my contact number xxxxxx.', 'uid-1609180623', 'pid-1609325271', 'order_placed', 4, 'Car is good.'),
('1609330939', '2021-01-01 15:32:51', 'yesss', 'ok hreeee', 'uid-1609180623', 'pid-1609330913', 'order_placed', 5, 'okokokok'),
('1609414840', '2021-01-01 15:16:42', 'Dear Seller, I would like to buy your Bike on hand to cash. Contact me.', 'Ok here is my contact details: 985671254 contact me for buying this bike.', 'uid-1609180623', 'pid-1609414758', NULL, 3, 'sajan'),
('1609521814', '2021-01-01 17:24:41', 'oadjaoindoia', 'ok contact number is here:5468541', 'uid-1608659204', 'pid-1609521772', NULL, 5, 'It was a great deal. mobile was on condition.'),
('1609956273', '2021-01-06 18:07:58', 'orderrrr', 'okkkkk', 'uid-1609180623', 'pid-1609956246', NULL, 4, 'It is good'),
('1609956850', '2021-01-06 18:14:45', 'aaaaaa', 'aaaaaaaa', 'uid-1609180623', 'pid-1609956825', NULL, 0, 'aaa'),
('1609957061', '2021-01-06 18:24:40', 'i need', 'okk', 'uid-1609180623', 'pid-1609957035', NULL, 1, 'kkkk'),
('1609957687', '2021-01-06 18:29:11', 'aaaaaa', 'aaaa', 'uid-1609180623', 'pid-1609957652', NULL, 1, 'aaaa');

-- --------------------------------------------------------

--
-- Table structure for table `purchases`
--

CREATE TABLE `purchases` (
  `purchase_id` varchar(30) NOT NULL,
  `date` int(11) NOT NULL,
  `buyer_id` varchar(30) NOT NULL,
  `item_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `saved_ads`
--

CREATE TABLE `saved_ads` (
  `id` int(15) NOT NULL,
  `uid` varchar(30) NOT NULL,
  `ad_id` varchar(30) NOT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `saved_ads`
--

INSERT INTO `saved_ads` (`id`, `uid`, `ad_id`, `date`) VALUES
(11, 'uid-1602835691', 'pid-1606466010', '2021-01-06 17:54:48'),
(12, 'uid-1603199592', 'pid-1609956246', '2021-01-06 18:06:45');

-- --------------------------------------------------------

--
-- Table structure for table `seller_feedback`
--

CREATE TABLE `seller_feedback` (
  `satisfaction_score` int(11) NOT NULL,
  `buyer_comment` int(11) NOT NULL,
  `seller_id` varchar(30) NOT NULL,
  `item_id` varchar(30) NOT NULL,
  `purchase_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sold_status`
--

CREATE TABLE `sold_status` (
  `sold_status_id` varchar(30) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sold_status`
--

INSERT INTO `sold_status` (`sold_status_id`, `description`) VALUES
('order_accepted', NULL),
('order_delivered', NULL),
('order_placed', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `userreg`
--

CREATE TABLE `userreg` (
  `id` int(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `address_district` varchar(20) NOT NULL,
  `address_area` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userreg`
--

INSERT INTO `userreg` (`id`, `first_name`, `last_name`, `email`, `password`, `mobile_no`, `address_district`, `address_area`) VALUES
(117659, 'Sajan', 'Pyatha', 'admin', 'sajan', '9860590345', 'Bhaktapur', 'Thimi'),
(152067, 'Gyurme', 'Sherpa', 'gyurme@gmail.com', 'gyurme', '8749824799', 'Boudha', 'Chahbil'),
(304650, 'Sandis', 'Prajapati', 'sajandis.prajapati@gmail.com', 'sandis', '9898989898', 'Kathmandu', 'Bansbari');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` varchar(30) NOT NULL,
  `f_name` varchar(20) NOT NULL,
  `l_name` varchar(20) NOT NULL,
  `primary_email` varchar(50) NOT NULL,
  `password` varchar(256) NOT NULL,
  `contact_no` varchar(10) NOT NULL,
  `address` varchar(256) NOT NULL,
  `varification_doc_count` int(11) DEFAULT NULL,
  `varification_status` int(11) DEFAULT NULL,
  `sale_ids` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `f_name`, `l_name`, `primary_email`, `password`, `contact_no`, `address`, `varification_doc_count`, `varification_status`, `sale_ids`) VALUES
('uid-1602835691', 'gyurme', 'sherpa', 'gyurme.sp@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '014875264', 'boudha', NULL, NULL, ''),
('uid-1603128626', 'sajan', 'pyatha', 'sajanpathya@gmail.com', '7167c9b6c70495d4346cd24bed9691d684376552dd506c60152e1f4c482880cb', '1651465', 'hvjkbk', NULL, NULL, ''),
('uid-1603199592', 'Ritesh', 'Rai', 'ritesh@gmail.com', '84696d86eb6abe2025ed83e54fd192202917fe6a305c989fff0a616e300680b1', '456415', 'Chahbil', NULL, NULL, ''),
('uid-1608391869', 'Bishal', 'Shrestha', 'bishal@gmail.com', '9cb18b422c54cc80c27bd5c3f332a53360a9d5f876d5ca6a34a504ba25643926', '7849561161', 'Patan', NULL, NULL, ''),
('uid-1608659204', 'Asik', 'Karmacharya', 'asik@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '989898989', 'Hetauda', NULL, NULL, NULL),
('uid-1608918638', 'Bishu', 'Prajapati', 'bishu@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '999999999', 'gggg', NULL, NULL, NULL),
('uid-1609180623', 'Ram', 'Sharma', 'ram99@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '849645', 'kkkkkk', NULL, NULL, NULL),
('uid-1630745640', 'bijay', 'kumar', 'bijay@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '4548778787', 'ffs', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_varification`
--

CREATE TABLE `user_varification` (
  `document_name` int(11) NOT NULL,
  `document_image_blob` int(11) NOT NULL,
  `user_id` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment_types`
--
ALTER TABLE `comment_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `item_category`
--
ALTER TABLE `item_category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `item_comments`
--
ALTER TABLE `item_comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `comment_type_id` (`comment_type_id`),
  ADD KEY `commenter` (`commenter`),
  ADD KEY `reply_to` (`reply_to`);

--
-- Indexes for table `on_sale_items`
--
ALTER TABLE `on_sale_items`
  ADD PRIMARY KEY (`item_id`),
  ADD UNIQUE KEY `accepted_order_id` (`accepted_order_id`),
  ADD KEY `seller` (`seller`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `on_sale_items_ibfk_4` (`sold_status`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `purchases`
--
ALTER TABLE `purchases`
  ADD PRIMARY KEY (`purchase_id`),
  ADD KEY `buyer_id` (`buyer_id`),
  ADD KEY `item_id` (`item_id`);

--
-- Indexes for table `saved_ads`
--
ALTER TABLE `saved_ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ad_id` (`ad_id`),
  ADD KEY `uid` (`uid`);

--
-- Indexes for table `seller_feedback`
--
ALTER TABLE `seller_feedback`
  ADD KEY `seller_id` (`seller_id`),
  ADD KEY `item_id` (`item_id`),
  ADD KEY `purchase_id` (`purchase_id`);

--
-- Indexes for table `sold_status`
--
ALTER TABLE `sold_status`
  ADD PRIMARY KEY (`sold_status_id`);

--
-- Indexes for table `userreg`
--
ALTER TABLE `userreg`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `primary_email` (`primary_email`);

--
-- Indexes for table `user_varification`
--
ALTER TABLE `user_varification`
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `saved_ads`
--
ALTER TABLE `saved_ads`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `userreg`
--
ALTER TABLE `userreg`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=304651;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `item_comments`
--
ALTER TABLE `item_comments`
  ADD CONSTRAINT `item_comments_ibfk_1` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `item_comments_ibfk_2` FOREIGN KEY (`comment_type_id`) REFERENCES `comment_types` (`type_id`),
  ADD CONSTRAINT `item_comments_ibfk_3` FOREIGN KEY (`commenter`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `item_comments_ibfk_4` FOREIGN KEY (`reply_to`) REFERENCES `item_comments` (`comment_id`);

--
-- Constraints for table `on_sale_items`
--
ALTER TABLE `on_sale_items`
  ADD CONSTRAINT `on_sale_items_ibfk_1` FOREIGN KEY (`seller`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `on_sale_items_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `item_category` (`category_id`),
  ADD CONSTRAINT `on_sale_items_ibfk_3` FOREIGN KEY (`accepted_order_id`) REFERENCES `orders` (`order_id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `on_sale_items_ibfk_4` FOREIGN KEY (`sold_status`) REFERENCES `sold_status` (`sold_status_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `purchases`
--
ALTER TABLE `purchases`
  ADD CONSTRAINT `purchases_ibfk_1` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `purchases_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`);

--
-- Constraints for table `saved_ads`
--
ALTER TABLE `saved_ads`
  ADD CONSTRAINT `saved_ads_ibfk_1` FOREIGN KEY (`ad_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `saved_ads_ibfk_2` FOREIGN KEY (`uid`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `seller_feedback`
--
ALTER TABLE `seller_feedback`
  ADD CONSTRAINT `seller_feedback_ibfk_1` FOREIGN KEY (`seller_id`) REFERENCES `users` (`user_id`),
  ADD CONSTRAINT `seller_feedback_ibfk_2` FOREIGN KEY (`item_id`) REFERENCES `on_sale_items` (`item_id`),
  ADD CONSTRAINT `seller_feedback_ibfk_3` FOREIGN KEY (`purchase_id`) REFERENCES `purchases` (`purchase_id`);

--
-- Constraints for table `user_varification`
--
ALTER TABLE `user_varification`
  ADD CONSTRAINT `user_varification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);
--
-- Database: `farm_ug`
--
CREATE DATABASE IF NOT EXISTS `farm_ug` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `farm_ug`;

-- --------------------------------------------------------

--
-- Table structure for table `animals`
--

CREATE TABLE `animals` (
  `animal` varchar(50) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `animals`
--

INSERT INTO `animals` (`animal`, `description`) VALUES
('any_animal_bird', NULL),
('chicken', NULL),
('cow', NULL),
('duck', NULL),
('water buffallo', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `animal_related_issues`
--

CREATE TABLE `animal_related_issues` (
  `issue_id` int(10) NOT NULL,
  `animal` varchar(50) NOT NULL,
  `issue` varchar(100) NOT NULL,
  `symptoms_text` text DEFAULT NULL,
  `symptoms_json` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `animal_related_issues`
--

INSERT INTO `animal_related_issues` (`issue_id`, `animal`, `issue`, `symptoms_text`, `symptoms_json`) VALUES
(1, 'any_animal_bird', 'Unconfirmed Issue', '', NULL),
(2, 'cow', 'Johne\'s Disease', 'diarrhea, weight loss (even with normal appetite), decrease in milk production, infertility, and eventually death. The first sign in dairy cattle is often declining milk production.', NULL),
(3, 'cow', 'Pink Eye', 'Once introduced to the eye, the bacteria causes irritation and tearing. The first sign of a pink eye infection in cattle is squinting. Very soon after the initial infection, the cornea begins to cloud up and soon becomes completely white. An ulcer will form on the cornea and can cause permanent blindness if not treated.', NULL),
(4, 'cow', 'Brucellosis', 'the disease localizes in the reproductive organs and/or the udder. Bacteria are shed in milk, aborted fetuses, afterbirth, or other reproductive tract discharges. There is no way to detect infected animals by their appearance. The most obvious signs in pregnant animals are abortion or birth of weak calves. Not all infected cows abort, but those that do usually abort between the fifth and seventh month of gestation. Milk production may be reduced from changes in the normal lactation cycle caused by abortions and delayed conception. Calves from infected cows may have infections that are not detected until they become pregnant, abort, or give birth themselves.', NULL),
(5, 'chicken', 'Colibacillosis', 'The symptoms vary with the different types of infections. In the acute septicemic form, mortality may begin suddenly and progress rapidly. Morbidity may not be apparent and birds in apparently good condition may die. However, in most cases birds are listless with ruffled feathers and indications of fever. Additional symptoms of labored breathing, occasional coughing and rales may be apparent. Diarrhea may be evident. Mortality may be high in recently hatched chicks and poults as a result of navel infection of coliforms.', NULL),
(6, 'chicken', 'Bird Flu', NULL, NULL),
(13, 'duck', 'bird flu', '1. fever\n2. death', NULL),
(14, 'water buffallo', 'ecoli', '1. fever', NULL),
(15, 'cow', 'Cancer', '1. uncontrolled growth', NULL),
(16, 'water buffallo', 'cancer', '1. swelling', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `experts`
--

CREATE TABLE `experts` (
  `user_id` varchar(25) NOT NULL,
  `primary_email` varchar(50) NOT NULL,
  `password` varchar(65) NOT NULL,
  `f_name` varchar(50) NOT NULL,
  `expertise_type` varchar(30) NOT NULL,
  `contact_no` varchar(15) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `experts`
--

INSERT INTO `experts` (`user_id`, `primary_email`, `password`, `f_name`, `expertise_type`, `contact_no`, `address`) VALUES
('uid-1623325245', 'gs@gmail.com', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', 'Jack Daniels', 'Vetnerian', '985468954', 'Kathmandu');

-- --------------------------------------------------------

--
-- Table structure for table `farm_details`
--

CREATE TABLE `farm_details` (
  `farm_id` varchar(15) NOT NULL,
  `farm_email` varchar(50) NOT NULL,
  `farm_name` varchar(100) NOT NULL,
  `farm_pwd` varchar(65) NOT NULL,
  `farm_phone` varchar(15) NOT NULL,
  `farm_google_plus_code` varchar(20) DEFAULT NULL,
  `district` varchar(50) NOT NULL,
  `lat_cor` varchar(10) DEFAULT NULL,
  `lon_cor` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `farm_details`
--

INSERT INTO `farm_details` (`farm_id`, `farm_email`, `farm_name`, `farm_pwd`, `farm_phone`, `farm_google_plus_code`, `district`, `lat_cor`, `lon_cor`) VALUES
('f1623309353424', 'gyurme.sp@gmail.com', 'Gyurme Sherpa', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '6589523', 'FV5P+9V Handikhola', 'Kathmandu', '27.4584375', '84.8849988'),
('f1623310320123', 'dawasherpa912@gmail.com', 'Tenzi', '03ac674216f3e15c761ee1a5e255f067953623c8b388b4459e13f978d7c846f4', '9849537409', 'FVCR+M6 Hetauda', 'Okhaldhunga ', '27.4716875', '84.8883738'),
('f1623392364275', 'j@gmail.com', 'Jacky Sheeps', 'a665a45920422f9d417e4867efdc4fb8a04a1f3fff1fa07e998e86f7f7a27ae3', '9965254349', '', 'Lalitpur', '27.6576506', '85.2763448');

-- --------------------------------------------------------

--
-- Table structure for table `reported_animal_related_issues`
--

CREATE TABLE `reported_animal_related_issues` (
  `issue_report_id` int(10) NOT NULL,
  `reporter_farm_id` varchar(15) NOT NULL,
  `animal` varchar(50) NOT NULL,
  `reporter_suspected_issue` varchar(100) NOT NULL,
  `reported_symptoms` text DEFAULT NULL,
  `expert_issue_confirmation` varchar(100) DEFAULT NULL,
  `report_subject` text DEFAULT NULL,
  `reported_at` datetime NOT NULL,
  `incident_of_or_since` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `reported_animal_related_issues`
--

INSERT INTO `reported_animal_related_issues` (`issue_report_id`, `reporter_farm_id`, `animal`, `reporter_suspected_issue`, `reported_symptoms`, `expert_issue_confirmation`, `report_subject`, `reported_at`, `incident_of_or_since`) VALUES
(8, 'f1623310320123', 'chicken', 'Bird Flu', '', ' Bird Flu ', 'due to rotten food, and some dirty environment ', '2021-06-10 13:22:18', '2021-06-10'),
(9, 'f1623309353424', 'duck', 'Unconfirmed Issue', 'sudden fever and death', '13', 'seems like new kind of disease', '2021-06-10 14:42:09', '2021-06-10'),
(10, 'f1623309353424', 'water buffallo', 'Unconfirmed Issue', 'swelling of the belly', ' ecoli ', 'bloating', '2021-06-10 14:44:01', '2021-06-10'),
(11, 'f1623392364275', 'water buffallo', 'Unconfirmed Issue', '', ' ecoli ', 'massive bloats', '2021-06-11 12:06:02', '2021-06-11'),
(12, 'f1623392364275', 'cow', 'Unconfirmed Issue', 'fghjf', ' Brucellosis ', '', '2021-06-11 15:51:41', '2021-06-11');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `trans_id` int(11) NOT NULL,
  `farm_id` varchar(15) NOT NULL,
  `date_time` datetime NOT NULL,
  `transaction_type` varchar(100) NOT NULL,
  `bill` varchar(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `reciept_amount` decimal(10,0) NOT NULL,
  `payment_amount` decimal(10,0) NOT NULL,
  `explanation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`trans_id`, `farm_id`, `date_time`, `transaction_type`, `bill`, `name`, `reciept_amount`, `payment_amount`, `explanation`) VALUES
(14, 'f1623310320123', '2021-06-10 13:19:20', 'Sale', 'no', 'Pet house', '500000', '0', ''),
(15, 'f1623392364275', '2021-06-11 12:05:34', 'Sale', 'no', 'sheeps', '56893', '0', ''),
(16, 'f1623392364275', '2021-06-11 14:45:54', 'Sale', 'yes', 'bought goats', '4520', '0', ''),
(17, 'f1623392364275', '2021-06-11 14:47:49', 'Sale', 'no', 'goats', '41235', '0', '10 medium goats'),
(18, 'f1623392364275', '2021-06-11 14:49:04', 'Payment', 'no', 'land rent', '0', '20000', ''),
(19, 'f1623392364275', '2021-06-11 15:52:36', 'Payment', 'no', 'fgh', '0', '58866', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `animals`
--
ALTER TABLE `animals`
  ADD PRIMARY KEY (`animal`);

--
-- Indexes for table `animal_related_issues`
--
ALTER TABLE `animal_related_issues`
  ADD PRIMARY KEY (`issue_id`),
  ADD KEY `animal` (`animal`);

--
-- Indexes for table `experts`
--
ALTER TABLE `experts`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`primary_email`);

--
-- Indexes for table `farm_details`
--
ALTER TABLE `farm_details`
  ADD PRIMARY KEY (`farm_id`),
  ADD UNIQUE KEY `farm_email` (`farm_email`);

--
-- Indexes for table `reported_animal_related_issues`
--
ALTER TABLE `reported_animal_related_issues`
  ADD PRIMARY KEY (`issue_report_id`),
  ADD KEY `farm_id` (`reporter_farm_id`),
  ADD KEY `animal` (`animal`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`trans_id`),
  ADD KEY `farm_id` (`farm_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `animal_related_issues`
--
ALTER TABLE `animal_related_issues`
  MODIFY `issue_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `reported_animal_related_issues`
--
ALTER TABLE `reported_animal_related_issues`
  MODIFY `issue_report_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `trans_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`farm_id`) REFERENCES `farm_details` (`farm_id`);
--
-- Database: `grecords`
--
CREATE DATABASE IF NOT EXISTS `grecords` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `grecords`;
--
-- Database: `phpmyadmin`
--
CREATE DATABASE IF NOT EXISTS `phpmyadmin` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin;
USE `phpmyadmin`;

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE `pma__bookmark` (
  `id` int(11) NOT NULL,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks';

-- --------------------------------------------------------

--
-- Table structure for table `pma__central_columns`
--

CREATE TABLE `pma__central_columns` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_length` text COLLATE utf8_bin DEFAULT NULL,
  `col_collation` varchar(64) COLLATE utf8_bin NOT NULL,
  `col_isNull` tinyint(1) NOT NULL,
  `col_extra` varchar(255) COLLATE utf8_bin DEFAULT '',
  `col_default` text COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Central list of columns';

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE `pma__column_info` (
  `id` int(5) UNSIGNED NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `input_transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_settings`
--

CREATE TABLE `pma__designer_settings` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `settings_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Settings related to Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__export_templates`
--

CREATE TABLE `pma__export_templates` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `export_type` varchar(10) COLLATE utf8_bin NOT NULL,
  `template_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `template_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved export templates';

-- --------------------------------------------------------

--
-- Table structure for table `pma__favorite`
--

CREATE TABLE `pma__favorite` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Favorite tables';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE `pma__history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp(),
  `sqlquery` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__navigationhiding`
--

CREATE TABLE `pma__navigationhiding` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `item_type` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Hidden items of navigation tree';

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) UNSIGNED NOT NULL,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{\"db\":\"farm_ug\",\"table\":\"animal_related_issues\"},{\"db\":\"farm_ug\",\"table\":\"experts\"},{\"db\":\"farm_ug\",\"table\":\"animals\"},{\"db\":\"farm_ug\",\"table\":\"farm_details\"},{\"db\":\"ecommerce\",\"table\":\"item_comments\"},{\"db\":\"ecommerce\",\"table\":\"on_sale_items\"},{\"db\":\"ecommerce\",\"table\":\"users\"},{\"db\":\"ecommerce\",\"table\":\"comment_types\"},{\"db\":\"farm_ug\",\"table\":\"transactions\"},{\"db\":\"farm_ug\",\"table\":\"reported_animal_related_issues\"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

--
-- Dumping data for table `pma__relation`
--

INSERT INTO `pma__relation` (`master_db`, `master_table`, `master_field`, `foreign_db`, `foreign_table`, `foreign_field`) VALUES
('farm_ug', 'animal_related_issues', 'animal', 'farm_ug', 'animals', 'animal'),
('farm_ug', 'reported_animal_related_issues', 'animal', 'farm_ug', 'animals', 'animal'),
('farm_ug', 'reported_animal_related_issues', 'expert_issue_confirmation', 'farm_ug', 'animal_related_issues', 'issue_id'),
('farm_ug', 'reported_animal_related_issues', 'reporter_farm_id', 'farm_ug', 'farm_details', 'farm_id'),
('farm_ug', 'reported_animal_related_issues', 'reporter_suspected_issue', 'farm_ug', 'animal_related_issues', 'issue_id');

-- --------------------------------------------------------

--
-- Table structure for table `pma__savedsearches`
--

CREATE TABLE `pma__savedsearches` (
  `id` int(5) UNSIGNED NOT NULL,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `search_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Saved searches';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT 0,
  `x` float UNSIGNED NOT NULL DEFAULT 0,
  `y` float UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

--
-- Dumping data for table `pma__table_info`
--

INSERT INTO `pma__table_info` (`db_name`, `table_name`, `display_field`) VALUES
('farm_ug', 'animal_related_issues', 'animal'),
('farm_ug', 'reported_animal_related_issues', 'reporter_farm_id'),
('farm_ug', 'transactions', 'farm_id');

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'farm_ug', 'reported_animal_related_issues', '{\"sorted_col\":\"`reported_animal_related_issues`.`issue_report_id` ASC\"}', '2021-06-10 14:44:33');

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin DEFAULT NULL,
  `data_sql` longtext COLLATE utf8_bin DEFAULT NULL,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) UNSIGNED NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `config_data` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2021-09-04 10:55:36', '{\"Console\\/Mode\":\"collapse\"}');

-- --------------------------------------------------------

--
-- Table structure for table `pma__usergroups`
--

CREATE TABLE `pma__usergroups` (
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL,
  `tab` varchar(64) COLLATE utf8_bin NOT NULL,
  `allowed` enum('Y','N') COLLATE utf8_bin NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User groups with configured menu items';

-- --------------------------------------------------------

--
-- Table structure for table `pma__users`
--

CREATE TABLE `pma__users` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `usergroup` varchar(64) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Users and their assignments to user groups';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pma__central_columns`
--
ALTER TABLE `pma__central_columns`
  ADD PRIMARY KEY (`db_name`,`col_name`);

--
-- Indexes for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`);

--
-- Indexes for table `pma__designer_settings`
--
ALTER TABLE `pma__designer_settings`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_user_type_template` (`username`,`export_type`,`template_name`);

--
-- Indexes for table `pma__favorite`
--
ALTER TABLE `pma__favorite`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__history`
--
ALTER TABLE `pma__history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `username` (`username`,`db`,`table`,`timevalue`);

--
-- Indexes for table `pma__navigationhiding`
--
ALTER TABLE `pma__navigationhiding`
  ADD PRIMARY KEY (`username`,`item_name`,`item_type`,`db_name`,`table_name`);

--
-- Indexes for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  ADD PRIMARY KEY (`page_nr`),
  ADD KEY `db_name` (`db_name`);

--
-- Indexes for table `pma__recent`
--
ALTER TABLE `pma__recent`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__relation`
--
ALTER TABLE `pma__relation`
  ADD PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  ADD KEY `foreign_field` (`foreign_db`,`foreign_table`);

--
-- Indexes for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_savedsearches_username_dbname` (`username`,`db_name`,`search_name`);

--
-- Indexes for table `pma__table_coords`
--
ALTER TABLE `pma__table_coords`
  ADD PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`);

--
-- Indexes for table `pma__table_info`
--
ALTER TABLE `pma__table_info`
  ADD PRIMARY KEY (`db_name`,`table_name`);

--
-- Indexes for table `pma__table_uiprefs`
--
ALTER TABLE `pma__table_uiprefs`
  ADD PRIMARY KEY (`username`,`db_name`,`table_name`);

--
-- Indexes for table `pma__tracking`
--
ALTER TABLE `pma__tracking`
  ADD PRIMARY KEY (`db_name`,`table_name`,`version`);

--
-- Indexes for table `pma__userconfig`
--
ALTER TABLE `pma__userconfig`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `pma__usergroups`
--
ALTER TABLE `pma__usergroups`
  ADD PRIMARY KEY (`usergroup`,`tab`,`allowed`);

--
-- Indexes for table `pma__users`
--
ALTER TABLE `pma__users`
  ADD PRIMARY KEY (`username`,`usergroup`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pma__bookmark`
--
ALTER TABLE `pma__bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__column_info`
--
ALTER TABLE `pma__column_info`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__export_templates`
--
ALTER TABLE `pma__export_templates`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__history`
--
ALTER TABLE `pma__history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__pdf_pages`
--
ALTER TABLE `pma__pdf_pages`
  MODIFY `page_nr` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pma__savedsearches`
--
ALTER TABLE `pma__savedsearches`
  MODIFY `id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Database: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `test`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
